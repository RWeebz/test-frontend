import React from "react";
import Footer from "../../components/Footer";
import Header from "../../components/Header";

type Props = {
  children: JSX.Element;
};

const HorizontalLayout: React.FC<Props> = ({ children }) => {
  return (
    <div className="theme-dark">
      <div id="main" className="layout-horizontal">
        <Header />
        <div className="content-wrapper container">{children}</div>
        <Footer />
      </div>
    </div>
  );
};

export default HorizontalLayout;

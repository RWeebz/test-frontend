import Link from "next/link";
import React, { useEffect, useState } from "react";
import { BsFacebook, BsInstagram, BsTwitter, BsYoutube } from "react-icons/bs";
import { FaApple, FaGooglePlay } from "react-icons/fa";

type Props = {
  children: JSX.Element;
};

const LandingLayout: React.FC = ({}) => {
  const [scrollPosition, setScrollPosition] = useState(
    (typeof window !== "undefined" && window.scrollY) || 0,
  );

  useEffect(() => {
    window.addEventListener("scroll", onScroll.bind(this));
    return () => {
      window.removeEventListener("scroll", onScroll.bind(this));
    };
  }, []);

  const onScroll = () => {
    requestAnimationFrame(() => {
      setScrollPosition(window.scrollY);
    });
  };

  const showBlur = scrollPosition > 0;

  return (
    <>
      <header
        id="header"
        onScroll={onScroll}
        className={`fixed-top  header-transparent ${showBlur ? "header-scrolled" : ""}`}
      >
        <div className="container d-flex align-items-center justify-content-between">
          <div className="logo">
            <h1>
              <Link href={"/"}>
                <a>Payr</a>
              </Link>
            </h1>
            <a href="index.html">
              {/* <img src="/images/logo/logo.png" alt="" className="img-fluid" /> */}
            </a>
          </div>

          <nav id="navbar" className="navbar">
            <ul>
              <li>
                <a className="nav-link scrollto active" href="#hero">
                  Home
                </a>
              </li>
              <li>
                <Link href={"/signup"}>
                  <a className="nav-link scrollto">Sign Up</a>
                </Link>
              </li>
              <li>
                <Link href={"/login"}>
                  <a className="getstarted scrollto">Log In</a>
                </Link>
              </li>
            </ul>
            <i className="bi bi-list mobile-nav-toggle"></i>
          </nav>
        </div>
      </header>
      <section id="hero" className="d-flex align-items-center">
        <div className="container">
          <div className="row">
            <div
              className="col-lg-6 d-lg-flex flex-lg-column justify-content-center align-items-stretch pt-5 pt-lg-0 order-2 order-lg-1"
              data-aos="fade-up"
            >
              <div>
                <h1>Pay anything, anywhere easier</h1>
                <br />
                <h2>
                  Enjoy convenient transactions,
                  <br />
                  for your daily needs
                </h2>
                <a href="#" className="download-btn">
                  <FaGooglePlay
                    style={{ position: "absolute", left: "18px", top: "11.5px" }}
                  />{" "}
                  Google Play
                </a>
                <a href="#" className="download-btn">
                  <FaApple
                    style={{ position: "absolute", fontSize: "20px", left: "18px" }}
                  />{" "}
                  App Store
                </a>
              </div>
            </div>
            <div
              className="col-lg-6 d-lg-flex flex-lg-column align-items-stretch order-1 order-lg-2 hero-img"
              data-aos="fade-up"
            >
              <img src="/images/hero-img.png" className="img-fluid" alt="" />
            </div>
          </div>
        </div>
      </section>

      <footer id="footer">
        <div className="footer-newsletter">
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-lg-6">
                <h4>Subscribe to Us </h4>
                <form action="" method="post">
                  <input type="email" name="email" />
                  <input type="submit" value="Subscribe" />
                </form>
              </div>
            </div>
          </div>
        </div>

        <div className="footer-top">
          <div className="container">
            <div className="row">
              <div className="col-lg-3 col-md-6 footer-contact">
                <h3>Appland</h3>
              </div>

              <div className="col-lg-3 col-md-6 footer-links">
                <h4>Useful Links</h4>
                <ul>
                  <li>
                    <i className="bx bx-chevron-right"></i> <a href="#">Home</a>
                  </li>
                  <li>
                    <i className="bx bx-chevron-right"></i> <a href="#">About us</a>
                  </li>
                  <li>
                    <i className="bx bx-chevron-right"></i> <a href="#">Services</a>
                  </li>
                  <li>
                    <i className="bx bx-chevron-right"></i>{" "}
                    <a href="#">Terms of service</a>
                  </li>
                  <li>
                    <i className="bx bx-chevron-right"></i> <a href="#">Privacy policy</a>
                  </li>
                </ul>
              </div>

              <div className="col-lg-3 col-md-6 footer-links">
                <h4>Career</h4>
                <ul>
                  <li>
                    <i className="bx bx-chevron-right"></i> <a href="#">Student</a>
                  </li>
                  <li>
                    <i className="bx bx-chevron-right"></i> <a href="#">Professional</a>
                  </li>
                </ul>
              </div>

              <div className="col-lg-3 col-md-6 footer-links">
                <h4>Connect with us</h4>
                <div className="social-links mx-auto">
                  <a href="#" className="twitter">
                    <BsTwitter />
                  </a>
                  <a href="#" className="facebook">
                    <BsFacebook />
                  </a>
                  <a href="#" className="instagram">
                    <BsInstagram />
                  </a>
                  <a href="#" className="youtube">
                    <BsYoutube />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="container py-4">
          <div className="copyright">
            &copy; Copyright{" "}
            <strong>
              <span>Payr</span>
            </strong>
            . All Rights Reserved
          </div>
          <div className="credits"></div>
        </div>
      </footer>
    </>
  );
};

export default LandingLayout;

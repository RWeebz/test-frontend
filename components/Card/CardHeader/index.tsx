import React from "react";

type Props = {
  children: JSX.Element | JSX.Element[];
  className?: string;
};

const CardHeader: React.FC<Props & React.HTMLProps<HTMLDivElement>> = (props) => {
  return (
    <div {...props} className={`card-header ${props.className}`}>
      {props.children}
    </div>
  );
};

export default CardHeader;

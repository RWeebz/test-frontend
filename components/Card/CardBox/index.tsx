import React from "react";

type Props = {
  children: JSX.Element | JSX.Element[];
  className?: string;
};

const CardBox: React.FC<Props & React.HTMLProps<HTMLDivElement>> = ({
  children,
  className,
  ...props
}) => {
  return (
    <div {...props} className={`card ${className}`}>
      {children}
    </div>
  );
};

export default CardBox;

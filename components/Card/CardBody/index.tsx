import React from "react";

type Props = {
  children: JSX.Element | JSX.Element[];
  className?: string;
};

const CardBody: React.FC<Props & React.HTMLProps<HTMLDivElement>> = ({
  children,
  className,
  ...props
}) => {
  return (
    <div {...props} className={`card-body ${className}`}>
      {children}
    </div>
  );
};

export default CardBody;

import Link from "next/link";
import { BsPerson, BsShieldLock } from "react-icons/bs";
import Styles from "../auth.module.css";

export default function Login() {
  return (
    <div id={Styles["auth"]}>
      <div className="row h-100">
        <div className="col-lg-5 col-12">
          <div id={Styles["auth-left"]}>
            <div className={Styles["auth-logo"]}>
              <Link href="/">
                <a href="/">
                  <img src="/images/logo/logo.png" alt="Logo" />
                </a>
              </Link>
            </div>
            <h1 className={Styles["auth-title"]}>Log in.</h1>
            <p className={`${Styles["auth-subtitle"]} mb-5`}>
              Log in with your data that you entered during registration.
            </p>

            <form action="/dashboard">
              <div className="form-group position-relative has-icon-left mb-4">
                <input
                  type="text"
                  className="form-control form-control-xl"
                  placeholder="Username"
                />
                <div className="form-control-icon">
                  <BsPerson />
                </div>
              </div>
              <div className="form-group position-relative has-icon-left mb-4">
                <input
                  type="password"
                  className="form-control form-control-xl"
                  placeholder="Password"
                />
                <div className="form-control-icon">
                  <BsShieldLock />
                </div>
              </div>
              <div className="form-check form-check-lg d-flex align-items-end">
                <input
                  className="form-check-input me-2"
                  type="checkbox"
                  value=""
                  id="flexCheckDefault"
                />
                <label
                  className="form-check-label text-gray-600"
                  htmlFor="flexCheckDefault"
                >
                  Keep me logged in
                </label>
              </div>
              <button className="btn btn-primary btn-block btn-lg shadow-lg mt-5">
                Log in
              </button>
            </form>
            <div className="text-center mt-5 text-lg fs-4">
              <p className="text-gray-600">
                Don't have an account?{" "}
                <Link href="/signup">
                  <a className="font-bold">Sign up</a>
                </Link>
                .
              </p>
              <p>
                <a className="font-bold" href="auth-forgot-password.html">
                  Forgot password?
                </a>
                .
              </p>
            </div>
          </div>
        </div>
        <div className="col-lg-7 d-none d-lg-block">
          <div id={Styles["auth-right"]}></div>
        </div>
      </div>
    </div>
  );
}

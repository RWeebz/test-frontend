import Link from "next/link";
import { BsEnvelope } from "react-icons/bs";
import Styles from "../auth.module.css";

export default function forgotPassowrd() {
  return (
    <div id={Styles["auth"]}>
      <div className="row h-100">
        <div className="col-lg-5 col-12">
          <div id={Styles["auth-left"]}>
            <div className={Styles["auth-logo"]}>
              <a href="index.html">
                <img src="images/logo/logo.png" alt="Logo" />
              </a>
            </div>
            <h1 className={Styles["auth-title"]}>Forgot Password</h1>
            <p className={`${Styles["auth-subtitle"]} mb-5`}>
              Input your email and we will send you reset password link.
            </p>

            <form action="index.html">
              <div className="form-group position-relative has-icon-left mb-4">
                <input
                  type="email"
                  className="form-control form-control-xl"
                  placeholder="Email"
                />
                <div className="form-control-icon">
                  <BsEnvelope />
                </div>
              </div>
              <button className="btn btn-primary btn-block btn-lg shadow-lg mt-5">
                Send
              </button>
            </form>
            <div className="text-center mt-5 text-lg fs-4">
              <p className="text-gray-600">
                Remember your account?{" "}
                <Link href="/login">
                  <a className="font-bold">Log in</a>
                </Link>
                .
              </p>
            </div>
          </div>
        </div>
        <div className="col-lg-7 d-none d-lg-block">
          <div id={Styles["auth-right"]}></div>
        </div>
      </div>
    </div>
  );
}

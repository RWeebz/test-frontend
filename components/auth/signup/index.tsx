import Link from "next/link";
import Styles from "../auth.module.css";
import { BsEnvelope, BsPerson, BsShieldLock } from "react-icons/bs";

export default function SignUp() {
  return (
    <div id={Styles["auth"]}>
      <div className="row h-100">
        <div className="col-lg-5 col-12">
          <div id={Styles["auth-left"]}>
            <div className={Styles["auth-logo"]}>
              <Link href="/">
                <a href="/">
                  <img src="/images/logo/logo.png" alt="Logo" />
                </a>
              </Link>
            </div>
            <h1 className={Styles["auth-title"]}>Sign Up</h1>
            <p className={`${Styles["auth-subtitle"]} mb-5`}>
              Input your data to register to our website.
            </p>
            <form action="index.html">
              <div className="form-group position-relative has-icon-left mb-4">
                <input
                  type="text"
                  className="form-control form-control-xl"
                  placeholder="Email"
                />
                <div className="form-control-icon">
                  <BsEnvelope />
                </div>
              </div>
              <div className="form-group position-relative has-icon-left mb-4">
                <input
                  type="text"
                  className="form-control form-control-xl"
                  placeholder="Username"
                />
                <div className="form-control-icon">
                  <BsPerson />
                </div>
              </div>
              <div className="form-group position-relative has-icon-left mb-4">
                <input
                  type="password"
                  className="form-control form-control-xl"
                  placeholder="Password"
                />
                <div className="form-control-icon">
                  <BsShieldLock />
                </div>
              </div>
              <div className="form-group position-relative has-icon-left mb-4">
                <input
                  type="password"
                  className="form-control form-control-xl"
                  placeholder="Confirm Password"
                />
                <div className="form-control-icon">
                  <BsShieldLock />
                </div>
              </div>
              <button className="btn btn-primary btn-block btn-lg shadow-lg mt-5">
                Sign Up
              </button>
            </form>
            <div className="text-center mt-5 text-lg fs-4">
              <p className="text-gray-600">
                Already have an account?{" "}
                <Link href="/login">
                  <a className="font-bold">Log in</a>
                </Link>
                .
              </p>
            </div>
          </div>
        </div>
        <div className="col-lg-7 d-none d-lg-block">
          <div id={Styles["auth-right"]}></div>
        </div>
      </div>
    </div>
  );
}

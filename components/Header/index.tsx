import Link from "next/link";
import React, { useState } from "react";
import { BsGrid, BsStack, BsJustify } from "react-icons/bs";
import { IconType } from "react-icons/lib";
import Menu from "../Menu/Menu";
import NavBar from "../NavBar";

const navBarItem: {
  icon: IconType;
  name: string;
  link: string;
  hasSub?: boolean;
  subMenu?: { name: string; link: string }[];
}[] = [
  {
    icon: BsGrid,
    name: "Dashboard",
    link: "/",
    hasSub: false,
    subMenu: [],
  },
  {
    icon: BsStack,
    name: "Components",
    link: "/",
    hasSub: true,
    subMenu: [{ name: "Alert", link: "http://google.com" }],
  },
];

const Header: React.FC = () => {
  const styleForDropdownUser: React.CSSProperties = {
    margin: "0px",
    position: "absolute",
    inset: "0px auto auto 0px",
    transform: "translate3d(-107px, 40px, 0px)",
  };
  const [toggleUser, setToggleUser] = useState<boolean>(false);

  const handleToggle = () => {
    setToggleUser(!toggleUser);
  };

  return (
    <header className="mb-5">
      <div className="header-top">
        <div className="container">
          <div className="logo">
            <Link href={"/"}>
              <a>
                <img src={"/images/logo/logo.png"} alt="Logo" srcSet="" />
              </a>
            </Link>
          </div>
          <div className="header-top-right">
            <div>
              <div className="dropdown" onClick={() => handleToggle()}>
                <a
                  href="#"
                  className={`user-dropdown d-flex dropend ${toggleUser ? `show` : ``}`}
                >
                  <div className="avatar avatar-md2">
                    <img src="/images/faces/1.jpg" alt="Avatar" />
                  </div>
                  <div className="text">
                    <h6 className="user-dropdown-name">John Ducky</h6>
                    <p className="user-dropdown-status text-sm text-muted">Member</p>
                  </div>
                </a>
                <ul
                  className={`dropdown-menu dropdown-menu-end shadow-lg ${
                    toggleUser ? `show` : ``
                  }`}
                  aria-labelledby="dropdownMenuButton1"
                  style={toggleUser ? styleForDropdownUser : { margin: 0 }}
                >
                  <li>
                    <a className="dropdown-item" href="#">
                      My Account
                    </a>
                  </li>
                  <li>
                    <a className="dropdown-item" href="#">
                      Settings
                    </a>
                  </li>
                  <li>
                    <hr className="dropdown-divider" />
                  </li>
                  <li>
                    <a className="dropdown-item" href="auth-login.html">
                      Logout
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <NavBar>
        <Menu navBarItem={navBarItem} />
      </NavBar>
    </header>
  );
};

export default Header;

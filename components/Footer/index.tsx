import React from "react";
import { BsHeart } from "react-icons/bs";

const Footer: React.FC = () => {
  return (
    <footer>
      <div className="container">
        <div className="footer clearfix mb-0 text-muted">
          <div className="float-start">
            <p>2021 &copy; Mazer</p>
          </div>
          <div className="float-end">
            <p>
              Crafted with{" "}
              <span className="text-danger">
                <BsHeart />
              </span>{" "}
              by <a href="http://ahmadsaugi.com">A. Saugi</a>
            </p>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;

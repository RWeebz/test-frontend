import { FaBookmark, FaEye, FaUser, FaUserAlt, FaUserPlus } from "react-icons/fa";
import { IconType } from "react-icons/lib";
import CardBody from "../Card/CardBody";
import CardBox from "../Card/CardBox";
import CardHeader from "../Card/CardHeader";
import dynamic from "next/dynamic";
import { Props as ChartProps } from "react-apexcharts";
import { BsCircleFill } from "react-icons/bs";

interface ProfileVisitData {
  annotations: ChartProps["options"]["annotations"];
  dataLabels: ChartProps["options"]["dataLabels"];
  chart: ChartProps["options"]["chart"];
  fill: ChartProps["options"]["fill"];
  plotOptions: ChartProps["options"]["plotOptions"];
  series: ChartProps["series"];
  colors: ChartProps["options"]["colors"];
  xaxis: ChartProps["options"]["xaxis"];
}

interface WorldChartData {
  series: ChartProps["series"];
  chart: ChartProps["options"]["chart"];
  colors: ChartProps["options"]["colors"];
  stroke: ChartProps["options"]["stroke"];
  grid: ChartProps["options"]["grid"];
  dataLabels: ChartProps["options"]["dataLabels"];
  xaxis: ChartProps["options"]["xaxis"];
  show: boolean;
  yaxis: ChartProps["options"]["yaxis"];
  tooltip: ChartProps["options"]["tooltip"];
}

interface ChartData {
  profile: ProfileVisitData;
  europe: WorldChartData;
  america: WorldChartData;
  indonesia: WorldChartData;
}

const listCards: {
  cardClassName: string;
  name: string;
  body: string;
  iconClassName: string;
  icon: IconType;
}[] = [
  {
    cardClassName: "px-3 py-4-5",
    name: "Profile Views",
    body: "112.000",
    iconClassName: "stats-icon purple",
    icon: FaEye,
  },
  {
    cardClassName: "px-3 py-4-5",
    name: "Followers",
    body: "183.000",
    icon: FaUser,
    iconClassName: "stats-icon blue",
  },
  {
    cardClassName: "px-3 py-4-5",
    name: "Following",
    body: "80.000",
    icon: FaUserPlus,
    iconClassName: "stats-icon green",
  },
  {
    cardClassName: "px-3 py-4-5",
    name: "Saved Post",
    body: "112",
    icon: FaBookmark,
    iconClassName: "stats-icon red",
  },
];

const getRndInteger = (min: number, max: number) => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

const dataChart: ChartData = {
  profile: {
    annotations: {
      position: "back",
    },
    dataLabels: {
      enabled: false,
    },
    chart: {
      type: "bar",
      height: 300,
    },
    fill: {
      opacity: 1,
    },
    plotOptions: {},
    series: [
      {
        name: "sales",
        data: [
          getRndInteger(0, 100),
          getRndInteger(0, 100),
          getRndInteger(0, 100),
          getRndInteger(0, 100),
          getRndInteger(0, 100),
          getRndInteger(0, 100),
          getRndInteger(0, 100),
          getRndInteger(0, 100),
          getRndInteger(0, 100),
          getRndInteger(0, 100),
          getRndInteger(0, 100),
          getRndInteger(0, 100),
        ],
      },
    ],
    colors: ["#435ebe"],
    xaxis: {
      categories: [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec",
      ],
    },
  },
  europe: {
    series: [
      {
        name: "series1",
        data: [310, 800, 600, 430, 540, 340, 605, 805, 430, 540, 340, 605],
      },
    ],
    chart: {
      height: 80,
      type: "area",
      toolbar: {
        show: false,
      },
    },
    colors: ["#5350e9"],
    stroke: {
      width: 2,
    },
    grid: {
      show: false,
    },
    dataLabels: {
      enabled: false,
    },
    xaxis: {
      type: "datetime",
      categories: [
        "2018-09-19T00:00:00.000Z",
        "2018-09-19T01:30:00.000Z",
        "2018-09-19T02:30:00.000Z",
        "2018-09-19T03:30:00.000Z",
        "2018-09-19T04:30:00.000Z",
        "2018-09-19T05:30:00.000Z",
        "2018-09-19T06:30:00.000Z",
        "2018-09-19T07:30:00.000Z",
        "2018-09-19T08:30:00.000Z",
        "2018-09-19T09:30:00.000Z",
        "2018-09-19T10:30:00.000Z",
        "2018-09-19T11:30:00.000Z",
      ],
      axisBorder: {
        show: false,
      },
      axisTicks: {
        show: false,
      },
      labels: {
        show: false,
      },
    },
    show: false,
    yaxis: {
      labels: {
        show: false,
      },
    },
    tooltip: {
      x: {
        format: "dd/MM/yy HH:mm",
      },
    },
  },
  america: {
    series: [
      {
        name: "series1",
        data: [310, 800, 600, 430, 540, 340, 605, 805, 430, 540, 340, 605],
      },
    ],
    chart: {
      height: 80,
      type: "area",
      toolbar: {
        show: false,
      },
    },
    colors: ["#008b75"],
    stroke: {
      width: 2,
    },
    grid: {
      show: false,
    },
    dataLabels: {
      enabled: false,
    },
    xaxis: {
      type: "datetime",
      categories: [
        "2018-09-19T00:00:00.000Z",
        "2018-09-19T01:30:00.000Z",
        "2018-09-19T02:30:00.000Z",
        "2018-09-19T03:30:00.000Z",
        "2018-09-19T04:30:00.000Z",
        "2018-09-19T05:30:00.000Z",
        "2018-09-19T06:30:00.000Z",
        "2018-09-19T07:30:00.000Z",
        "2018-09-19T08:30:00.000Z",
        "2018-09-19T09:30:00.000Z",
        "2018-09-19T10:30:00.000Z",
        "2018-09-19T11:30:00.000Z",
      ],
      axisBorder: {
        show: false,
      },
      axisTicks: {
        show: false,
      },
      labels: {
        show: false,
      },
    },
    show: false,
    yaxis: {
      labels: {
        show: false,
      },
    },
    tooltip: {
      x: {
        format: "dd/MM/yy HH:mm",
      },
    },
  },
  indonesia: {
    series: [
      {
        name: "series1",
        data: [310, 800, 600, 430, 540, 340, 605, 805, 430, 540, 340, 605],
      },
    ],
    chart: {
      height: 80,
      type: "area",
      toolbar: {
        show: false,
      },
    },
    colors: ["#dc3545"],
    stroke: {
      width: 2,
    },
    grid: {
      show: false,
    },
    dataLabels: {
      enabled: false,
    },
    xaxis: {
      type: "datetime",
      categories: [
        "2018-09-19T00:00:00.000Z",
        "2018-09-19T01:30:00.000Z",
        "2018-09-19T02:30:00.000Z",
        "2018-09-19T03:30:00.000Z",
        "2018-09-19T04:30:00.000Z",
        "2018-09-19T05:30:00.000Z",
        "2018-09-19T06:30:00.000Z",
        "2018-09-19T07:30:00.000Z",
        "2018-09-19T08:30:00.000Z",
        "2018-09-19T09:30:00.000Z",
        "2018-09-19T10:30:00.000Z",
        "2018-09-19T11:30:00.000Z",
      ],
      axisBorder: {
        show: false,
      },
      axisTicks: {
        show: false,
      },
      labels: {
        show: false,
      },
    },
    show: false,
    yaxis: {
      labels: {
        show: false,
      },
    },
    tooltip: {
      x: {
        format: "dd/MM/yy HH:mm",
      },
    },
  },
};

const ChartComponent = dynamic(() => import("../Chart"), { ssr: false });

export default function Home() {
  return (
    <div className="page-content">
      <section className="row">
        <div className="col-12 col-lg-9">
          <div className="row">
            {listCards.map((val) => (
              <div className="col-6 col-lg-3 col-md-6" key={val.name}>
                <CardBox>
                  <CardBody className={val.cardClassName}>
                    <div className="row">
                      <div className="col-md-4">
                        <div className={val.iconClassName}>
                          <val.icon />
                        </div>
                      </div>
                      <div className="col-md-8">
                        <h6 className="text-muted font-semibold">{val.name}</h6>
                        <h6 className="font-extrabold mb-0">{val.body}</h6>
                      </div>
                    </div>
                  </CardBody>
                </CardBox>
              </div>
            ))}
          </div>
          <div className="row">
            <div className="col-12">
              <CardBox>
                <CardHeader>
                  <h4>Profile Visit</h4>
                </CardHeader>
                <CardBody>
                  <ChartComponent
                    height={dataChart.profile.chart.height}
                    type={dataChart.profile.chart.type}
                    series={dataChart.profile.series}
                    options={{
                      annotations: dataChart.profile.annotations,
                      dataLabels: dataChart.profile.dataLabels,
                      fill: dataChart.profile.fill,
                      xaxis: dataChart.profile.xaxis,
                    }}
                  />
                </CardBody>
              </CardBox>
            </div>
          </div>
          <div className="row">
            <div className="col-12 col-xl-4">
              <CardBox>
                <CardHeader>
                  <h4>Profile Visit</h4>
                </CardHeader>
                <CardBody>
                  <div className="row">
                    <div className="col-6">
                      <div className="d-flex align-items-center">
                        <BsCircleFill
                          className="bi text-primary"
                          width={32}
                          height={32}
                          fill="blue"
                          style={{ width: "10px" }}
                        />
                        <h5 className="mb-0 ms-3">Europe</h5>
                      </div>
                    </div>
                    <div className="col-6">
                      <h5 className="mb-0">862</h5>
                    </div>
                    <div className="col-12">
                      <ChartComponent
                        height={dataChart.europe.chart.height}
                        type={dataChart.europe.chart.type}
                        series={dataChart.europe.series}
                        options={{
                          tooltip: dataChart.europe.tooltip,
                          yaxis: dataChart.europe.yaxis,
                          xaxis: dataChart.europe.xaxis,
                          dataLabels: dataChart.europe.dataLabels,
                          grid: dataChart.europe.grid,
                          stroke: dataChart.europe.stroke,
                          colors: dataChart.europe.colors,
                        }}
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-6">
                      <div className="d-flex align-items-center">
                        <BsCircleFill
                          className="bi text-success"
                          width={32}
                          height={32}
                          fill={"green"}
                          style={{ width: "10px" }}
                        />
                        <h5 className="mb-0 ms-3">America</h5>
                      </div>
                    </div>
                    <div className="col-6">
                      <h5 className="mb-0">375</h5>
                    </div>
                    <div className="col-12">
                      <ChartComponent
                        height={dataChart.america.chart.height}
                        type={dataChart.america.chart.type}
                        series={dataChart.america.series}
                        options={{
                          tooltip: dataChart.america.tooltip,
                          yaxis: dataChart.america.yaxis,
                          xaxis: dataChart.america.xaxis,
                          dataLabels: dataChart.america.dataLabels,
                          grid: dataChart.america.grid,
                          stroke: dataChart.america.stroke,
                          colors: dataChart.america.colors,
                        }}
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-6">
                      <div className="d-flex align-items-center">
                        <BsCircleFill
                          className="bi text-danger"
                          width={32}
                          height={32}
                          fill="red"
                          style={{ width: "10px" }}
                        />
                        <h5 className="mb-0 ms-3">Indonesia</h5>
                      </div>
                    </div>
                    <div className="col-6">
                      <h5 className="mb-0">1025</h5>
                    </div>
                    <div className="col-12">
                      <ChartComponent
                        height={dataChart.indonesia.chart.height}
                        type={dataChart.indonesia.chart.type}
                        series={dataChart.indonesia.series}
                        options={{
                          tooltip: dataChart.indonesia.tooltip,
                          yaxis: dataChart.indonesia.yaxis,
                          xaxis: dataChart.indonesia.xaxis,
                          dataLabels: dataChart.indonesia.dataLabels,
                          grid: dataChart.indonesia.grid,
                          stroke: dataChart.indonesia.stroke,
                          colors: dataChart.indonesia.colors,
                        }}
                      />
                    </div>
                  </div>
                </CardBody>
              </CardBox>
            </div>
            <div className="col-12 col-xl-8">
              <CardBox>
                <CardHeader>
                  <h4>Latest Comments</h4>
                </CardHeader>
                <CardBody>
                  <div className="table-responsive">
                    <table className="table table-hover table-lg">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Comment</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td className="col-3">
                            <div className="d-flex align-items-center">
                              <div className="avatar avatar-md">
                                <img src="/images/faces/5.jpg" />
                              </div>
                              <p className="font-bold ms-3 mb-0">Cantik</p>
                            </div>
                          </td>
                          <td className="col-auto">
                            <p className=" mb-0">Congratulations on your graduation!</p>
                          </td>
                        </tr>
                        <tr>
                          <td className="col-3">
                            <div className="d-flex align-items-center">
                              <div className="avatar avatar-md">
                                <img src="/images/faces/2.jpg" />
                              </div>
                              <p className="font-bold ms-3 mb-0">Ganteng</p>
                            </div>
                          </td>
                          <td className="col-auto">
                            <p className=" mb-0">
                              Wow amazing design! Can you make another tutorial for this
                              design?
                            </p>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </CardBody>
              </CardBox>
            </div>
          </div>
        </div>
        <div className="col-12 col-lg-3">
          <CardBox>
            <CardBody className="py-4 px-5">
              <div className="d-flex align-items-center">
                <div className="avatar avatar-xl">
                  <img src="/images/faces/1.jpg" alt="Face 1" />
                </div>
                <div className="ms-3 name">
                  <h5 className="font-bold">John Duck</h5>
                  <h6 className="text-muted mb-0">@johnducky</h6>
                </div>
              </div>
            </CardBody>
          </CardBox>

          <CardBox>
            <CardHeader>
              <h4>Recent Messages</h4>
            </CardHeader>
            <div className="card-content pb-4">
              <div className="recent-message d-flex px-4 py-3">
                <div className="avatar avatar-lg">
                  <img src="/images/faces/4.jpg" />
                </div>
                <div className="name ms-4">
                  <h5 className="mb-1">Hank Schrader</h5>
                  <h6 className="text-muted mb-0">@johnducky</h6>
                </div>
              </div>
              <div className="recent-message d-flex px-4 py-3">
                <div className="avatar avatar-lg">
                  <img src="/images/faces/5.jpg" />
                </div>
                <div className="name ms-4">
                  <h5 className="mb-1">Dean Winchester</h5>
                  <h6 className="text-muted mb-0">@imdean</h6>
                </div>
              </div>
              <div className="recent-message d-flex px-4 py-3">
                <div className="avatar avatar-lg">
                  <img src="/images/faces/1.jpg" />
                </div>
                <div className="name ms-4">
                  <h5 className="mb-1">John Dodol</h5>
                  <h6 className="text-muted mb-0">@dodoljohn</h6>
                </div>
              </div>
              <div className="px-4">
                <button className="btn btn-block btn-xl btn-light-primary font-bold mt-3">
                  Start Conversation
                </button>
              </div>
            </div>
          </CardBox>

          <CardBox>
            <CardHeader>
              <h4>Visitors Profile</h4>
            </CardHeader>
            <CardBody>
              <div id="chart-visitors-profile"></div>
            </CardBody>
          </CardBox>
        </div>
      </section>
    </div>
  );
}

import React from "react";

type Props = {
  children: JSX.Element;
};

const NavBar: React.FC<Props> = ({ children }) => {
  return (
    <nav className="main-navbar">
      <div className="container">{children}</div>
    </nav>
  );
};

export default NavBar;

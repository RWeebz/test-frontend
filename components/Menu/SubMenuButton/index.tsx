import React from "react";

type Props = {
  subMenu?: { name: string; link: string }[];
};

export const SubMenuButton: React.FC<Props> = ({ subMenu }) => {
  return (
    <>
      {subMenu.map((val) => {
        return (
          <li className="submenu-item" key={val.name}>
            <a href={val.link} className="submenu-link">
              {val.name}
            </a>
          </li>
        );
      })}
    </>
  );
};

export default SubMenuButton;

import Link from "next/link";
import React from "react";
import { BsGrid } from "react-icons/bs";
import { IconType } from "react-icons/lib";
import DropdownSubItem from "../SubMenuButton";

interface Props {
  navBarItem: {
    icon: IconType;
    name: string;
    link: string;
    hasSub?: boolean;
    subMenu?: { name: string; link: string }[];
  }[];
}

const Menu: React.FC<Props> = ({ navBarItem }) => {
  return (
    <ul>
      {navBarItem.map((val) => {
        return (
          <li className={`menu-item ${val.hasSub ? "has-sub" : ""}`} key={val.name}>
            <Link href="/">
              <a className="menu-link">
                <val.icon />
                <span>{val.name}</span>
              </a>
            </Link>
            {val.hasSub ? (
              <div className="submenu">
                <div className="submenu-group-wrapper">
                  <ul className="submenu-group">
                    <DropdownSubItem subMenu={val.subMenu} />
                  </ul>
                </div>
              </div>
            ) : (
              <></>
            )}
          </li>
        );
      })}
    </ul>
  );
};

export default Menu;

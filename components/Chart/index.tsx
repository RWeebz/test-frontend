import React from "react";
import ChartModule, { Props as ChartProps } from "react-apexcharts";

const Chart: React.FC<ChartProps> = (props) => {
  return <ChartModule {...props} />;
};

export default Chart;

import { GetStaticProps } from "next";
import Dashboard from "../components/Dashboard";
import HorizontalLayout from "../layout/Dashboard/HorizontalLayout";

export default function HomePage() {
  return (
    <HorizontalLayout>
      <Dashboard />
    </HorizontalLayout>
  );
}

export const getStaticProps: GetStaticProps = async () => {
  return {
    props: {},
  };
};
